-- [Section] Advanced Selects:

-- Exclude record from the Select Query
    SELECT * FROM songs WHERE id != 11;

    -- Selecting specific column from the table
    SELECT song_name, genre FROM songs WHERE id != 11;

-- Greater than or equal operator
SELECT * FROM songs WHERE id >= 11;

-- Less than or equal operator
SELECT * FROM songs WHERE id <= 11;

-- What if I want those records with id that has id 5 to 11:
SELECT * FROM songs WHERE id >= 5 AND id <= 11;
SELECT * FROM songs WHERE id BETWEEN 5 AND 11;

-- GET specific IDs:
SELECT * FROM songs WHERE id = 11;

-- OR Operator:
SELECT * FROM songs WHERE id = 1 OR id = 11 or id = 5;

-- IN Operator;
SELECT * FROM songs WHERE id IN (1, 11, 5);

-- Find Partial matches:
-- records end with letter
SELECT * FROM songs WHERE song_name LIKE "%a";
SELECT * FROM songs WHERE song_name LIKE "%e";

-- records starts with s
SELECT * FROM songs WHERE song_name LIKE "s%";

SELECT * FROM songs WHERE album_id LIKE "_0_";

-- SORT record;
-- Ascending
SELECT * FROM songs ORDER BY song_name ASC;

-- Descending
SELECT * FROM songs ORDER BY song_name DESC;

-- DISCTINCT Records:
SELECT DISTINCT genre FROM songs;

-- [SECTION] Table joins:
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- JOIN albums table and songs table:
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;