-- SECTION INSERT RECORD(CREATE);
    -- INSERT INTO table_name (columns_on_table) VALUES(values_per_column)

INSERT INTO artists(name) VALUES ('Rivermaya');
INSERT INTO artists(name) VALUES ('Psy');

-- DESCRIBE table_name;
    -- TO check the columns per tables.
-- SELECT * FROM table_name;
    -- To check the values of tables.

INSERT INTO albums (album_title, date_released, artist_id) VALUES ('Psy6', '2012-1-1', 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ('Trip', '1996-1-1', 1);

INSERT INTO songs(title, length, genre, album_id) VALUES ('Gangnam Style', 253, 'K-pop', 1);
INSERT INTO songs(title, length, genre, album_id) VALUES ('Kundiman', 234, 'OPM', 2);
INSERT INTO songs(title, length, genre, album_id) VALUES ('Kisapmata', 259, 'OPM', 2);

-- [Section] Selecting Records(Retrieve):
-- Retrieve all the records in a specific table
    -- SELECT * FROM table_name;
SELECT * FROM songs;

-- Retrieve all the records but not all the columns;
SELECT title, length FROM songs;

-- Retrieve record given a specific condition:
SELECT * FROM songs WHERE genre = 'OPM';

SELECT title , length FROM songs WHERE genre = 'OPM';

-- AND and OR keyword;
SELECT * FROM songs WHERE length > 240 OR genre = 'OPM';

-- [SECTION] UPDATING RECORD
-- UPDATE table_name SET column_name = value WHERE condition;

UPDATE songs SET length = 240;
UPDATE songs SET length = 259 WHERE id = 2;
UPDATE songs SET length = 300 WHERE length > 240 AND genre = 'OPM';
UPDATE songs SET length = 240 WHERE id = 2 AND genre = 'OPM';

-- [SECTION] DELETE Record/s
-- DELETE FROM table_name WHERE condition;
DELETE FROM songs WHERE genre = 'OPM' AND length = 240;
