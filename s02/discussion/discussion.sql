--SQL syntaxes are not case sensitive.
--If you are going to add a SQL syntax(capitalize).
--  If you are going to use or add name of the column or table(small letter).

-- To show all the list of our databases
SHOW DATABASES;

-- To create a database
CREATE DATABASE music_db;

-- to drop/delete a database:
DROP DATABASE trial_db;

-- Select a database.
USE music_db;

-- Show Tables inside database
SHOW TABLES;

-- Creating a tables:
CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
);

--Mini-activity: You are going to create the artists table:
CREATE TABLE artists(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

--To change the name of the table
ALTER TABLE current_table_name TO new_table_name;
RENAME TABLE current_table_name TO new_table_name;

CREATE TABLE albums(
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_albums_artists_id
        FOREIGN KEY(artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    GENRE VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_songs_albums_id
        FOREIGN KEY(album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    CONSTRAINT fk_playlist_users_id
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- multiple foreign key for 1 table

CREATE TABLE playlists_songs(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    CONSTRAINT fk_playlists_songs_playlists_id
        FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_songs_id
        FOREIGN KEY (song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);